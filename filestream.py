#!/usr/bin/env python
# ------------------------------------------
#
#  CIS457 - Data Communications
#  Project 3
#  GVSU 2013
#
#               **STARRING**
#     __     ______     ______     __  __
#    /\ \   /\  __ \   /\  ___\   /\ \/ /
#   _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
#  /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
#  \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
#    Josh       Aaron      Chris      Kyle
#
# ------------------------------------------

import math
import os
import packet


class FileStream:
    """
    Wrapper class for files so that the content of a file can be
    easily split into packet sized chunks.
    """
    def __init__(self, path):
        if os.path.exists(path):
            if os.path.isfile(path):
                self._path = path
            else:
                raise IOError('Path is a folder, not a file!')
        else:
            raise IOError('File does not exist!')

        self._stream = open(self.path, 'rb')


    def __str__(self):
        return '<%s (%s)>' % (self.__class__.__name__, os.path.basename(self._path))


    def __len__(self):
        """Get the number of chunks contained in the file"""
        return int(math.ceil(os.path.getsize(self._path) / float(packet.PACKETSIZE - packet.HEADER_LEN)))


    def __iter__(self):
        """Returns itself when iterating. See also: next()"""
        return self


    def next(self):
        """Called to iterate through file stream in PACKETSIZE - HEADER_LEN chunks"""
        chunk = self._stream.read(packet.PACKETSIZE - packet.HEADER_LEN)
        if chunk:
            return chunk
        else:
            self._stream.close()
            raise StopIteration()


    def close(self):
        """Performs cleanup operations for filestream"""
        self._stream.close()


    @property
    def path(self):
        """The path to the file being streaed"""
        return self._path


    @staticmethod
    def len(path):
        """
        Gives the same info a len(filstream_instance) but without
        opening the file or creating a new stream
        """
        return int(math.ceil(os.path.getsize(path) / float(packet.PACKETSIZE - packet.HEADER_LEN)))


if __name__ == '__main__':
    # Do some test junk
    fs = FileStream(__file__)
    print fs
    for chunk in fs:
        print '"%s"' % [chunk]
        # print '"%s"' % packet.string_to_bytes(chunk)
