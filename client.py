#!/usr/bin/env python
# ------------------------------------------
#
#  CIS457 - Data Communications
#  Project 3
#  GVSU 2013
#
#               **STARRING**
#     __     ______     ______     __  __
#    /\ \   /\  __ \   /\  ___\   /\ \/ /
#   _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
#  /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
#  \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
#    Josh       Aaron      Chris      Kyle
#
# ------------------------------------------

import argparse
import logging
import os
import socket
import sys
import time
import traceback

import packet
from packet import Packet


class Client:
    """
    A simple file server client that uses UDP with added robustness.
    """
    def __init__(self, server, port, timeout, attempts, winlen, rpath, lpath):
        self._server_ip = server
        self._server_port = port
        self._timeout = timeout
        self._attempts = attempts
        self._window_len = winlen
        self._remote_path = rpath
        self._local_path = lpath

        # Initial state is requesting
        self._state = 'req'

        # init logger
        self._log = logging.getLogger(self.__class__.__name__)

        # Attempt to bind socket
        try:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self._log.info('__init__(): Setting timeout: %ims' % self._timeout)
            self._socket.settimeout(timeout / 1000.0)
            self._log.info('__init__(): Started client. server=%s:%i, to=%ims, rt=%i' %
                          (self.server_ip, self.server_port, self._timeout, self._attempts))

            self._file = open(self._local_path, 'wb')
        except Exception:
            self._log.error('__init__(): Unable to start client:\n%s' % (traceback.format_exc()))
            sys.exit(-1)


    @property
    def server_port(self):
        """Get the port that the server is operating on"""
        return self._server_port


    @property
    def server_ip(self):
        """Get the IP address that the server is operating from"""
        return self._server_ip


    def shutdown(self):
        """Close the server socket, filestream and shutdown the server"""
        self._log.warn('Shutting down!')
        if not self._file.closed:
            self._file.close()
        self._socket.close()


    def run(self):
        """The main state machine loop to process transfers"""
        pkt = None
        # Sequence info
        seq_len = 0
        # Packet window
        packetwin = {}
        # Keep track of which packets have been received with <set> containing sequence numbers
        seq_set = set()
        # Timer
        start = 0

        # Begin state machine loop
        while True:
            # Client is requesting a file from the servers
            if self._state == 'req':
                self._log.info('run(%s): Requesting "%s" from %s...' %
                               (self._state, self._remote_path, self._server_ip))
                sent = self._send_packet(Packet.new(packet.REQ, packet.END, 0, 1, (self.server_ip, self.server_port), self._remote_path))
                if sent:
                    # Start timer
                    start = time.time()
                    # Go to confirm state
                    self._state = 'cnf'
                else:
                    self._log.error('run(%s): Maximum number of attempts (%s) exceeded! Giving up...' % (self._state, self._attempts))
                    self._state = 'done'

            # Client is waiting for confirmation of file
            elif self._state == 'cnf':
                pkt = self._get_packet()
                if not pkt:
                    self._log.error('run(%s): Server did not respond to request. Giving up...' % self._state)
                    self._state = 'done'
                else:
                    if pkt.is_cnf:
                        self._log.info('run(%s): Server confirmed request: %s' % (self._state, pkt.data))
                        seq_len = pkt.seq_len
                        self._state = 'get'
                    elif pkt.is_den:
                        self._log.error('run(%s): Server denied request: %s' % (self._state, pkt.data))
                        self._state = 'done'
                    else:
                        self._log.error('run(%s): Packet type is "%s", not CNF/DEN' % (self._state, packet.TYPE_MAP[pkt.pkt_type]))
                        self._state = 'req'

            # Client is receiving data
            elif self._state == 'get':
                if not seq_set:
                    self._log.info('run(%s): Beginning transfer from %s...' % (self._state, self._server_ip))

                pkt = None
                while not pkt:
                    pkt = self._get_packet()
                    if pkt:
                        self._log.debug('run(%s): Got %s' % (self._state, pkt))
                        if pkt.is_seq:
                            # Save packet data
                            if pkt.seq_num not in seq_set:
                                packetwin[pkt.seq_num] = pkt.data
                                seq_set.add(pkt.seq_num)
                            else:
                                # Packet was already received, send ACK immediately
                                self._send_packet(Packet.new(packet.ACK, packet.END, pkt.seq_num, pkt.seq_len, pkt.addr, 'pkt#%i OK' % pkt.seq_num))

                            # ACK packet
                            self._state = 'ack'
                            break
                        elif pkt.is_cnf:
                            # Ignore a random CNF packet
                            continue
                        else:
                            self._log.error('run(%s): Packet type is "%s", not SEQ!' % (self._state, packet.TYPE_MAP[pkt.pkt_type]))
                            # Report error to server
                            self._send_packet(Packet.new(packet.ERR, packet.END, 0, 1, pkt.addr,
                                                         'Invalid packet type: %s' % packet.TYPE_MAP[pkt.pkt_type]))
                    else:
                        # Attempt count exceeded, give up and exit
                        self._log.error('run(%s): Failed to receive data after %i tries. Giving up...' % (self._state, self._attempts))
                        # Report error to server
                        self._send_packet(Packet.new(packet.ERR, packet.END, 1, 1, (self.server_ip, self.server_port), 'Request timeout'))
                        self._state = 'done'
                        break

            # Client is sending acknowledgments
            elif self._state == 'ack':
                self._log.debug('run(%s): Sending ACK for %i of %i. ' % (self._state, pkt.seq_num, pkt.seq_len))
                sent = self._send_packet(Packet.new(packet.ACK, packet.END, pkt.seq_num, pkt.seq_len, pkt.addr, 'pkt#%i OK' % pkt.seq_num))
                if sent:
                    if len(seq_set) == seq_len:
                        self._log.info("run(%s): All packets received and ACK'd!" % self._state)
                    self._state = 'get'

                    if len(packetwin) == self._window_len or len(seq_set) == seq_len:
                        # Write window to disk if it is full or it is the last remaining partial window
                        self._log.debug('run(%s): Window full...' % self._state)
                        self._state = 'write'
                else:
                    # Attempt count exceeded, give up and exit
                    self._log.error('run(%s): Failed to send data after %i tries. Giving up...' % (self._state, self._attempts))
                    self._send_packet(Packet.new(packet.ERR, packet.END, 1, 1, pkt.addr, 'Unable to ACK'))
                    self._state = 'done'

            # Write window to disk
            elif self._state == 'write':
                self._log.debug('run(%s): Writing window (len=%i) to disk...' % (self._state, len(packetwin)))
                for n in sorted(packetwin.keys()):
                    self._file.write(packetwin[n])

                try:
                    progress = len(seq_set) / float(seq_len) * 100
                    tm = time.time() - start
                    speed = (len(seq_set) * float(packet.PACKETSIZE)) / 1024 / 1024 / tm
                    print '\rProgress: %.1lf%% done at %.3lf MBps' % (progress, speed),
                except ZeroDivisionError:
                    pass

                # Clear packet window
                packetwin = {}

                # Determine next state
                if len(seq_set) < seq_len:
                    # More data to come...
                    self._state = 'get'
                else:
                    # All packets in sequence have been received
                    self._log.info('run(%s): Transfer of complete! (%s -> %s)' % (self._state, self._remote_path, self._local_path))
                    self._state = 'done'
                    for i in xrange(self._attempts / 2):
                        self._send_packet(Packet.new(packet.BYE, packet.END, 1, 1, pkt.addr, 'TRANSFER COMPLETE'))

            # Done, nothing more to do
            elif self._state == 'done':
                self._log.info('run(%s): Exiting...' % self._state)
                break

            # Something has gone terribly wrong...
            else:
                self._log.error('run(%s): Unknown state: "%s"' % (self._state, self._state))
                self._state = 'done'


    def _get_packet(self, inf=False):
        """
        Receives a packet from the socket. Tries _attempts number of times before giving up.

        :param inf: If inf=True, this method will not return until a packet has been received

        :return: Packet object if success, False if not
        """
        tries = 0
        while True:
            p = None
            try:
                data, addr = self._socket.recvfrom(packet.PACKETSIZE)
                p = Packet(data, addr)
                break
            except socket.timeout:
                if not inf:
                    self._log.debug('_get_packet(): Timeout...')
                tries += 1
            except Exception:
                self._log.error('_get_packet(): Error:\n%s' % traceback.format_exc())
                break

            if not tries < self._attempts and not inf:
                self._log.error('_get_packet(): Failed to get packet within %i attempts!' % self._attempts)
                break
        return p


    def _send_packet(self, pkt):
        """
        Sends a packet out on the socket. Tries _attemts number of times before giving up.

        :param pkt: The Packet object to send

        :return: True if success, False if not
        """
        tries = 0
        while True:
            try:
                self._socket.sendto(pkt.payload, pkt.addr)
                return True
            except socket.timeout as e:
                self._log.debug('_send_packet(): Timeout...')
                tries += 1
            except Exception as e:
                self._log.error('_send_packet(): %s: %s' % (type(e).__name__, e))
                return False
            tries += 1
            if not tries < self._attempts:
                # Attempt count exceeded, give up and exit
                self._log.error('_send_packet(): Failed to send data after %i tries. Giving up...' % self._attempts)
                return False


if __name__ == '__main__':
    # Force unbuffered output to stdout and stderr
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
    sys.stderr = os.fdopen(sys.stderr.fileno(), 'w', 0)

    # init argument parser
    parser = argparse.ArgumentParser(description='UDPFS Client - J.A.C.K 2013')
    parser.add_argument('-p', '--port',
                        action='store',
                        type=int,
                        default=9876,
                        help='Port number to bind (1024 < port < 65535). default=9876')
    parser.add_argument('-w', '--window-size',
                        action='store',
                        type=int,
                        default=1024,
                        help='Sliding window size in packets. default=1024, min=5')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='Enable verbose logging.')
    parser.add_argument('-t', '--timeout',
                        action='store',
                        type=int,
                        default=1000,
                        help='REQ timeout period in milliseconds. default=1000ms')
    parser.add_argument('-a', '--attempts',
                        action='store',
                        type=int,
                        default=20,
                        help='Number of attempts to send request before giving up. default=20')
    parser.add_argument('-l', '--logfile',
                        action='store',
                        default=None,
                        help='Logfile location. No logfile is created if omitted.')
    parser.add_argument('server',
                        action='store',
                        default='localhost',
                        help='Server address. default=localhost')
    parser.add_argument('remote_path',
                        action='store',
                        help='File to request from server')
    parser.add_argument('local_path',
                        action='store',
                        help='Where to store the downloaded file')
    args = parser.parse_args()

    # init logging parameters
    cfg = {
        'level': logging.INFO if not args.verbose else logging.DEBUG,
        'format': '\r%(asctime)s %(levelname)-9s%(name)-10s%(message)s',
        'datefmt': '%m-%d-%Y %H:%M',
    }
    if args.logfile:
        cfg['filename'] = args.logfile
        cfg['filemode'] = 'w'

    # Perform configuration of logging module
    logging.basicConfig(**cfg)

    # If logfile is specified, we need an additional handler for the console
    if args.logfile:
        # define a Handler which writes INFO messages or higher to the console
        console = logging.StreamHandler(stream=sys.stdout)
        # set a format which is simpler for console use
        formatter = logging.Formatter('\r%(asctime)s %(levelname)-9s%(name)-10s%(message)s', '%m-%d-%Y %H:%M')
        # tell the handler to use this format
        console.setFormatter(formatter)
        # add the handler to the root logger
        logging.getLogger('').addHandler(console)

    # Get a logger object for __main__
    log = logging.getLogger('__main__')
    if args.logfile:
        log.info('Writing log info to "%s"' % args.logfile)

    # Check arguments
    if not 1024 < args.port < 65535:
        log.error('Invalid port! Must be 1024 < port < 65535!')
        sys.exit(-1)
    if not args.timeout > 0:
        log.error('Netagive/zero timeout not allowed!')
        sys.exit(-1)
    if not args.timeout > 0:
        log.error('Number of attempts must be greater than 0!')
        sys.exit(-1)
    if not args.window_size >= 5:
        log.error('Window size too small! must be >= 5 packets!')
        sys.exit(-1)

    # Start a client
    try:
        log.info('Starting Client - Python version %s on %s...' %
                 ('.'.join(["%s" % n for n in sys.version_info[:3]]), sys.platform))
        c = Client(args.server, args.port, args.timeout, args.attempts, args.window_size, args.remote_path, args.local_path)
        try:
            c.run()
            c.shutdown()
        except Exception as e:
            log.fatal('__main__(): Encountered Error:\n%s' % traceback.format_exc())
    except KeyboardInterrupt:
        c.shutdown()
