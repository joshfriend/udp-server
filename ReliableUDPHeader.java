import org.jnetpcap.packet.JHeader;
import org.jnetpcap.packet.JHeaderMap;
import org.jnetpcap.packet.annotate.Field;
import org.jnetpcap.packet.annotate.Header;

/**
 * *************************************************
 * class
 * <p/>
 * User: Carr, Christopher
 * Date: 4/5/13
 * 
 *   I don't know if this is the way to go. Got this notion
 * from an old blog at http://jnetpcap.com/node/105
 * <p/>
 * *************************************************
 */
@Header(name= "UdpReliable", nicname = "UdpRel")
public class ReliableUDPHeader extends JHeaderMap<JHeader>{

    @Field(offset = 0, length = 1, format = "%d")
    public int type() {
         return getUByte(0);
    }
}
