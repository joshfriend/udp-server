/**
 * Project 3 - Reliable UDP Packet Analyzer
 * CIS 457 - GVSU Winter 2013
 * Authors:
 *             **STARRING**
 * 	   __     ______     ______     __  __
 *    /\ \   /\  __ \   /\  ___\   /\ \/ /
 *   _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 *  /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 *  \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
 *    Josh       Aaron      Chris      Kyle
 *
 */
// Command to run at Windows cmd prompt
// java -cp .;jnetpcap.jar PacketAnalysis <server IP> <num_packets>

import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;
import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;

import java.nio.ByteBuffer;
import java.util.*;

public class PacketAnalysis {
	
	public static int NUM_PACKETS_TO_ANALYZE = 100;
	public static String serverAddress = "";
	
	public static void main (String args[]){
		/** MAIN-SCOPE VARIABLES **/
		
		if(args.length == 2)
		{
			NUM_PACKETS_TO_ANALYZE = Integer.parseInt(args[1]);
			serverAddress = args[0];
		}
		if(args.length == 1)
		{
			serverAddress = args[0];
		}
		if(args.length == 0 || args.length > 2)
		{
			System.out.println("Usage: \n" +
								"Server_IP_Address or DNS_Host_Name\n" +
								"<optional>Number of Packets to Analyze");
		}
		
		//create array list to store all captured packets
		ArrayList<PcapPacket> listOfPackets = new ArrayList<PcapPacket>(NUM_PACKETS_TO_ANALYZE+1);

		int ipv4 = 0;
		int numTCP = 0;
		int numUDP = 0;

		Ip4 ip = new Ip4();
		Tcp tcp = new Tcp();
		Udp udp = new Udp();
		
		//IPv4 protocol maps Key== "source_address / destination_address"
		Map<String, Integer> s2dBytes = new HashMap<String, Integer>();
		Map<String, Integer> s2dPackets = new HashMap<String, Integer>();

		double num_packets = (double) NUM_PACKETS_TO_ANALYZE;
		
		
		/** DISPLAY NETWORK INTERFACES AND HAVE USER CHOOSE WHICH TO USE **/
		System.out.println("DISPLAY NETWORK INTERFACES:");

		//create list of Pcap interfaces
		StringBuilder errbuf = new StringBuilder();
		List<PcapIf> ifs = new ArrayList<PcapIf>(); // Will hold list of devices
		int statusCode = Pcap.findAllDevs(ifs, errbuf);
		if (statusCode != Pcap.OK) {
			System.out.println("Error occured: " + errbuf.toString());
			return;
		}

		//display list of interfaces
		for (int i = 0; i < ifs.size(); i++) {
			System.out.println("#" + i + ": " + ifs.get(i).getDescription());
		}

		//read input from user - which interface to use
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter interface number: >"); 
		int input = scan.nextInt();
		System.out.println("you entered: " + input);
		PcapIf netInterface = ifs.get(input);
		scan.close();

		/** OPEN NETWORK INTERFACE **/
		//open up network interface specified by user (input)
		int snaplen = 2048; // Truncate packet at this size 
		int promiscous = Pcap.MODE_PROMISCUOUS;
		int timeout = 60 * 1000; // In milliseconds
		Pcap myPcap = Pcap.openLive(netInterface.getName(), snaplen, promiscous, timeout, errbuf);
				
		/** CREATE PACKET HANDLER **/
		//Packet handler to add packets to list
		PcapPacketHandler<ArrayList<PcapPacket> > handler = 
				new PcapPacketHandler<ArrayList<PcapPacket> >(){		
			public void nextPacket(PcapPacket packet, ArrayList<PcapPacket> listOfPackets){
				listOfPackets.add(packet);
				//if(packet.hasHeader(eth))
				//if(eth.type() > 0x600)
				//ethernet2++;
				//System.out.println("type: " + eth.type() + "\tdescrip: " + eth.typeDescription());
			}
		};
		
		/** CAPTURE PACKETS **/
		myPcap.loop(NUM_PACKETS_TO_ANALYZE, handler, listOfPackets);
		
		String source_address;
		String destination_address;
		
		byte[] payload;
		ByteBuffer buf;
		
		/** ANALYZE PACKETS **/
		//step through all packets
		for(PcapPacket p : listOfPackets){
			
			//CHECK FOR IPv4
			if(p.hasHeader(ip)){
				ipv4++;
				
				byte dest[] = ip.destination();
				byte sour[] = ip.source();
				source_address = org.jnetpcap.packet.format.FormatUtils.ip(sour);
				destination_address = org.jnetpcap.packet.format.FormatUtils.ip(dest);
				
				String ip_combo = source_address + " / " + destination_address;

				//add up bytes for specific source/destination
				if(s2dBytes.containsKey(ip_combo)){
					s2dBytes.put((ip_combo), 
							(s2dBytes.get(ip_combo)) + p.getTotalSize());
				}
				//if source/destination hasn't been seen yet, add it to map
				else
					s2dBytes.put(ip_combo, p.getTotalSize());

				//add up number of occurrences of source/destination pair
				if(s2dPackets.containsKey(ip_combo)){
					s2dPackets.put(ip_combo, 
							(s2dPackets.get(ip_combo))+1);
				}
				//if source/destination pair hasnt been seen yet, add it to map
				else
					s2dPackets.put(ip_combo, 1);
				
				//CHECK FOR TCP
				if(p.hasHeader(tcp)){
					numTCP++;
				}
				
				//CHECK FOR UDP
				if(p.hasHeader(udp))
				{					
//					System.out.printf("Source: %s Dest: %s\n", source_address, 
//							destination_address );
					if(source_address.trim().equals(serverAddress) || 
							destination_address.trim().equals(serverAddress))
					{
						numUDP++;
						payload = udp.getPayload();
						buf = ByteBuffer.wrap(payload);
						String type = new String(payload, 0, 2);
						String frame = new String(payload, 2, 2);
						//String seqNum = new String(payload, 4, 4);
						Integer seqNumber = new Integer(buf.get(4));
						//String seqLen = new String(payload, 8, 4);
						Integer seqLength = new Integer(buf.get(8));

						System.out.printf("Packet#%d Type:%s Frame:%s SeqNum:%d SeqLen:%d\n",
								numUDP, type, frame, seqNumber, seqLength);
					}
				}
			}
		}

		//Display IPv4 Statistics
		System.out.println("\n--------IPv4 Statistics---------");
        System.out.println("\nIPv4 packets: " + ipv4);

		System.out.printf("\n%s     / %s\tNum_Packets\t   Bytes\tPercent\n", " Source",
						"  Destination");
		Set<String> keys2 = s2dPackets.keySet();
		for(String key: keys2){
			System.out.printf("%-30s  %10d\t%8d\t", key.toString(), s2dPackets.get(key), 
							s2dBytes.get(key) );
			System.out.printf("%.1f", ((s2dPackets.get(key))/(double)num_packets * 100));
			System.out.println("%");
		}
		
		//Display TCP, UDP, and ICMP stats
		System.out.println("\n---------TCP, UDP, and ICMP Statistics---------");
		System.out.println("Type\tNum_Packets\tPercent");
		System.out.print("TCP\t" + numTCP + "\t\t");	
		System.out.printf("%.1f", (double)numTCP/ipv4*100);
		System.out.println("%");
		System.out.print("UDP\t" + numUDP + "\t\t");
		System.out.printf("%.1f", (double)numUDP/ipv4*100);
		System.out.println("%");
		
		myPcap.close();
		System.out.println("\n\nprogram completed");
	}
}
