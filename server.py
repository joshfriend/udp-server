#!/usr/bin/env python
# ------------------------------------------
#
#  CIS457 - Data Communications
#  Project 3
#  GVSU 2013
#
#               **STARRING**
#     __     ______     ______     __  __
#    /\ \   /\  __ \   /\  ___\   /\ \/ /
#   _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
#  /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
#  \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
#    Josh       Aaron      Chris      Kyle
#
# ------------------------------------------

import argparse
import logging
import os
import socket
import sys
import time
import traceback

from filestream import FileStream
import packet
from packet import Packet


class Server:
    """
    A simple file server that uses UDP with added robustness.
    """
    def __init__(self, port=9876, timeout=10, attempts=5, winsize=10):
        # Store parameters
        self._port = port
        self._timeout = timeout
        self._attempts = attempts
        self._window_size = winsize

        # Initial state is idle (waiting for requests)
        self._state = 'idle'

        # init logger
        self._log = logging.getLogger(self.__class__.__name__)

        # Attempt to bind socket
        try:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self._log.info('__init__(): Attempting to bind socket on port %i' % self.port)
            self._socket.bind(('', self.port))
            self._log.info('__init__(): Setting timeout: %ims' % self._timeout)
            self._socket.settimeout(timeout / 1000.0)
            self._log.info('__init__(): Started server on port %i, to=%ims, ws=%ipkts' %
                           (self.port, self._timeout, self._window_size))
        except Exception:
            self._log.error('__init__(): Unable to start server:\n%s' % (traceback.format_exc()))
            sys.exit(-1)


    def __str__(self):
        return '<UDPServer (port:%i)>' % self.port


    @property
    def port(self):
        """Get the port that the server is operating on"""
        return self._port


    def shutdown(self):
        """Close the server socket and shutdown the server"""
        self._log.warn('Shutting down!')
        self._socket.close()


    def run(self):
        """The main state machine loop to process transfers"""
        # Request packet
        req = None
        # Filestream
        fs = None
        fs_len = 0
        # Sequence counter
        seqn = 1
        # Packet window
        last_win = False
        packetwin = {}
        # Timekeeping
        start = 0

        # Begin state machine loop
        while True:
            # Server is waiting for new requests
            if self._state == 'idle':
                self._log.info('run(%s): Waiting for requests...' % self._state)

                # Reset state variables
                if fs:
                    fs.close()
                    fs = None
                seqn = 1
                last_win = False

                req = self._get_packet(inf=True)
                if req.is_req:
                    self._state = 'req'

            # Server has received a request and needs to check it
            elif self._state == 'req':
                if not req.is_req:
                    self._log.error('run(%s): Packet type is %s, not REQ!' % (self._state, packet.TYPE_MAP[req.pkt_type]))
                    self._send_packet(Packet.new(packet.ERR, packet.END, 1, 1, req.addr, 'ERROR: NOT A REQUEST!'))
                    self._state = 'idle'
                else:
                    self._log.info('run(%s): Request from %s: %s' % (self._state, req.addr[0], req.data))
                    ok, msg = self._check_request(req.data)
                    if ok:
                        # Confirm request
                        self._log.debug('_check_request(): %s' % msg)
                        fs_len = FileStream.len(req.data)
                        for i in xrange(self._attempts / 2):
                            self._send_packet(Packet.new(packet.CNF, packet.END, 0, fs_len, req.addr, msg))
                            # Go to transmission state
                        start = time.time()
                        self._state = 'send'
                        #else:
                        #    self._log.error('run(%s): Failed to send confirmation of request!' % self._state)
                        #    self._state = 'idle'
                    else:
                        # Deny request
                        self._log.info('run(%s): Invalid request for "%s": %s' % (self._state, req.data, msg))
                        self._send_packet(Packet.new(packet.DEN, packet.END, 1, 1, req.addr, msg))

                        # Go to idle state
                        self._state = 'idle'

            # Server has received a valid request and will begin sending the file data
            elif self._state == 'send':
                # Open file if not already open
                if not fs:
                    fs = FileStream(req.data)
                    fs_len = len(fs)
                    self._log.debug('run(%s): Filestream contains %ix %iB chunk(s).' % (self._state, len(fs), packet.PACKETSIZE))

                if not packetwin:
                    # Fill window
                    for i in range(self._window_size):
                        try:
                            packetwin[seqn + i] = fs.next()
                        except StopIteration:
                            break
                        except ValueError:
                            self._log.error('run(%s): Error building window:\n%s' % (self._state, traceback.format_exc()))
                            self._state = 'idle'
                            break
                    seqn += len(packetwin)
                else:
                    self._log.debug('run(%s): Retransmitting remaining window contents' % self._state)

                self._log.debug('run(%s): Sending packets: %s' % (self._state, packetwin.keys()))

                try:
                    progress = seqn / float(fs_len) * 100
                    tm = time.time() - start
                    speed = seqn * float(packet.PACKETSIZE) / 1024 / 1024 / tm
                    print '\rProgress: %.1lf%% done at %.3lf MBps' % (progress, speed),
                except ZeroDivisionError:
                    pass


                # Transmit window
                for n, data in packetwin.iteritems():
                    # Is this the last packet?
                    if n < fs_len:
                        frame = packet.CON
                    else:
                        frame = packet.END
                        last_win = True

                    # Send the packet
                    p = Packet.new(packet.SEQ, frame, n, fs_len, req.addr, data)
                    sent = self._send_packet(p)
                    if sent:
                        # seqn = max(seqn, n)
                        self._log.debug('run(%s): Sent: %s' % (self._state, p))
                        if last_win:
                            self._log.info('run(%s): Last window sent...' % self._state)
                    else:
                        self._log.error('run(%s): Failed to send data!' % self._state)
                        self._state = 'idle'

                # go to ACK check state
                self._log.debug('run(%s): Done transmitting window (len = %i).' % (self._state, len(packetwin)))
                self._state = 'wait'

            # Server has sent a window of data and is receiving acknowledgments from the client
            elif self._state == 'wait':
                # Wait for packets to ack
                self._log.debug('run(%s): Waiting for ACK(s)...' % self._state)

                while True:
                    if not packetwin:
                        # All packets ACK'd
                        self._log.debug("run(%s): All packets ACK'd" % self._state)
                        if last_win:
                            # Last window has been ACK'd, go to idle state
                            self._log.info("run(%s): Last window ACK'd. DONE!" % self._state)
                            self._state = 'idle'
                        else:
                            self._state = 'send'
                        break

                    p = self._get_packet()
                    if p:
                        if p.is_ack:
                            self._log.debug('run(%s): Received ACK: %s' % (self._state, p))
                            # remove from transmit window
                            try:
                                packetwin.pop(p.seq_num)
                            except KeyError:
                                self._log.debug("run(%s): Packet %i already ACK'd" % (self._state, p.seq_num))
                        elif p.is_err:
                            # Clientside error
                            self._log.error('run(%s): Client Error: "%s"' % (self._state, p.data))
                            self._state = 'idle'
                            break
                        elif p.is_bye and last_win:
                            # Client has sent Transmission complete
                            self._log.info('run(%s): Client sent BYE: %s' % (self._state, p))
                            self._state = 'idle'
                            break
                        else:
                            self._log.error('run(%s): Packet type is "%s", not "ACK". Msg: %s' % (self._state, packet.TYPE_MAP[p.pkt_type], p.data))
                            self._state = 'idle'
                            break
                    else:
                        self._log.debug('run(%s): Failed to receive ACK packet within %i tries.' % (self._state, self._attempts))
                        # Resend whats left of the window
                        # Go to idle state
                        self._state = 'send'
                        break

            # Something has gone terribly wrong...
            else:
                self._log.error('run(%s): Unknown state: "%s"' % (self._state, self._state))
                self._state = 'idle'


    def _check_request(self, req):
        """Checks if a file request is valid. Checks if file exists, is not a directory and is readable."""
        if os.path.exists(req):
            if os.path.isfile(req):
                try:
                    fp = open(req)
                    fp.close()
                    return True, 'Valid request'
                except Exception as e:
                    return False, '"%s" is not readable: (%s) %s' % (req, e.__class__.__name__, e)
            else:
                return False, '"%s" is not a file' % req
        else:
            return False, '"%s" Does not exist' % req


    def _get_packet(self, inf=False):
        """
        Receives a packet from the socket. Tries _attemts number of times before giving up.

        :param inf: If inf=True, this method will not return until a packet has been received

        :return: Packet object if success, False if not
        """
        tries = 0
        while True:
            p = None
            try:
                data, addr = self._socket.recvfrom(packet.PACKETSIZE)
                p = Packet(data, addr)
                break
            except socket.timeout:
                if not inf:
                    self._log.debug('_get_packet(): Timeout...')
                tries += 1
            except Exception:
                self._log.error('_get_packet(): Error:\n%s' % traceback.format_exc())
                break

            if not tries < self._attempts and not inf:
                self._log.error('_get_packet(): Failed to get packet within %i attempts!' % self._attempts)
                break
        return p


    def _send_packet(self, pkt):
        """
        Sends a packet out on the socket. Tries _attemts number of times before giving up.

        :param pkt: The Packet object to send

        :return: True if success, False if not
        """
        tries = 0
        while True:
            try:
                self._socket.sendto(pkt.payload, pkt.addr)
                return True
            except socket.timeout as e:
                self._log.debug('_send_packet(): Timeout...')
                tries += 1
            except Exception as e:
                self._log.error('_send_packet(): %s: %s' % (type(e).__name__, e))
                return False
            tries += 1
            if not tries < self._attempts:
                # Attempt count exceeded, give up and exit
                self._log.error('_send_packet(): Failed to send data after %i tries. Giving up...' % self._attempts)
                return False


if __name__ == '__main__':
    # Force unbuffered output to stdout and stderr
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
    sys.stderr = os.fdopen(sys.stderr.fileno(), 'w', 0)

    # init argument parser
    parser = argparse.ArgumentParser(description='UDPFS Server - J.A.C.K 2013')
    parser.add_argument('-p', '--port',
                        action='store',
                        type=int,
                        default=9876,
                        help='Port number to bind (1024 < port < 65535). default=9876')
    parser.add_argument('-t', '--timeout',
                        action='store',
                        type=int,
                        default=100,
                        help='ACK timeout period in milliseconds. default=100ms')
    parser.add_argument('-a', '--attempts',
                        action='store',
                        type=int,
                        default=20,
                        help='Number of attempts to send request before giving up. default=20')
    parser.add_argument('-w', '--window-size',
                        action='store',
                        type=int,
                        default=100,
                        help='Sliding window size in packets. default=100, min=5')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='Enable verbpse logging.')
    parser.add_argument('-l', '--logfile',
                        action='store',
                        default=None,
                        help='Logfile location. No logfile is created if omitted.')
    args = parser.parse_args()

    # init logging parameters
    cfg = {
        'level': logging.INFO if not args.verbose else logging.DEBUG,
        'format': '\r%(asctime)s %(levelname)-9s%(name)-10s%(message)s',
        'datefmt': '%m-%d-%Y %H:%M',
    }
    if args.logfile:
        cfg['filename'] = args.logfile
        cfg['filemode'] = 'w'

    # Perform configuration of logging module
    logging.basicConfig(**cfg)

    # If logfile is specified, we need an additional handler for the console
    if args.logfile:
        # define a Handler which writes INFO messages or higher to the console
        console = logging.StreamHandler(stream=sys.stdout)
        # set a format which is simpler for console use
        formatter = logging.Formatter('\r%(asctime)s %(levelname)-9s%(name)-10s%(message)s', '%m-%d-%Y %H:%M')
        # tell the handler to use this format
        console.setFormatter(formatter)
        # add the handler to the root logger
        logging.getLogger('').addHandler(console)

    # Get a logger object for __main__
    log = logging.getLogger('__main__')
    if args.logfile:
        log.info('Writing log info to "%s"' % args.logfile)

    # Check arguments
    if not 1024 < args.port < 65535:
        log.error('Invalid port! Must be 1024 < port < 65535!')
        sys.exit(-1)
    if not args.timeout > 0:
        log.error('Netagive timeout not allowed!')
        sys.exit(-1)
    if not args.timeout > 0:
        log.error('Number of attempts must be greater than 0!')
        sys.exit(-1)
    if not args.window_size >= 5:
        log.error('Window size too small! must be >= 5 packets!')
        sys.exit(-1)

    # Start a server
    try:
        log.info('Starting Server - Python version %s on %s...' %
                 ('.'.join(["%s" % n for n in sys.version_info[:3]]), sys.platform))
        s = Server(args.port, args.timeout, args.attempts, args.window_size)
        try:
            s.run()
            s.shutdown()
        except Exception as e:
            log.fatal('__main__(): Encountered Error:\n%s' % traceback.format_exc())
    except KeyboardInterrupt:
        s.shutdown()
