# ------------------------------------------
#
#  CIS457 - Data Communications
#  Project 3
#  GVSU 2013
#
#               **STARRING**
#     __     ______     ______     __  __
#    /\ \   /\  __ \   /\  ___\   /\ \/ /
#   _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
#  /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
#  \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
#    Josh       Aaron      Chris      Kyle
#
# ------------------------------------------

JFLAGS = -g -Xlint:unchecked
JC = javac
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = $(wildcard *.java)

default: classes

classes: $(CLASSES:.java=.class)

clean:
	$(RM) *.class
