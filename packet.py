#!/usr/bin/env python
# ------------------------------------------
#
#  CIS457 - Data Communications
#  Project 3
#  GVSU 2013
#
#               **STARRING**
#     __     ______     ______     __  __
#    /\ \   /\  __ \   /\  ___\   /\ \/ /
#   _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
#  /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
#  \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
#    Josh       Aaron      Chris      Kyle
#
# ------------------------------------------

import math
import struct

# TYPE CODES
REQ = '01'  # client request to server
CNF = '02'  # server confirmation - file exists
DEN = '03'  # server denial - file does not exist
SEQ = '04'  # packet is data with a sequence #
ACK = '05'  # packet is acknowledgment with ack'd sequence #
ERR = '06'  # unknown error occurred (end transmission)
BYE = '07'  # end of transfer

# FFRAME CODES
CON = '31'  # continue
END = '32'  # final packet, end data

# Map type/frame codes to human readable names
TYPE_MAP = {
    '01': 'REQ',
    '02': 'CNF',
    '03': 'DEN',
    '04': 'SEQ',
    '05': 'ACK',
    '06': 'ERR',
    '07': 'BYE',
    '31': 'CON',
    '32': 'END',
}

PACKETSIZE = 1024
HEADER_LEN = 12


class Packet:
    """
    A simple packet class that wraps data with info fields and flags to give
    UDP transmissions added robustness.
    """
    def __init__(self, data, addr):
        if len(data) <= PACKETSIZE:
            self._data = data
        else:
            raise TypeError('Packet data capacity exceeded! (len = %i)' % len(data))

        self._addr = addr


    def __str__(self):
        return '<%s: %s/%s, %i of %i>' % (self.__class__.__name__, TYPE_MAP[self.pkt_type],
                                          TYPE_MAP[self.frame_type], self.seq_num, self.seq_len)


    @property
    def addr(self):
        """Get the address tuple this packet was sent/recieved to/from"""
        return self._addr


    @property
    def data(self):
        """Raw packet data, minus the header bytes"""
        return self._data[HEADER_LEN:]


    @property
    def payload(self):
        """Raw packet data, including header bytes"""
        return self._data


    @property
    def pkt_type(self):
        """Extract the first pair of chars of data as a type flag field"""
        return self._data[0:2]


    @property
    def frame_type(self):
        """Extract the second pair of chars of data as a frame flag field"""
        return self._data[2:4]


    @property
    def seq_num(self):
        """Extract bytes 4-8 as a 32 bit sequence# field"""
        return struct.unpack('I', self._data[4:8])[0]


    @property
    def seq_len(self):
        """Extract bytes 8-12 as a 32 bit sequence length field"""
        return struct.unpack('I', self._data[8:12])[0]


    @property
    def is_req(self):
        """Is this packet a request?"""
        return self.pkt_type == REQ


    @property
    def is_cnf(self):
        """Is this packet a confirmation?"""
        return self.pkt_type == CNF


    @property
    def is_den(self):
        """Is this packet a denial?"""
        return self.pkt_type == DEN


    @property
    def is_seq(self):
        """Is this packet data in a sequence?"""
        return self.pkt_type == SEQ


    @property
    def is_ack(self):
        """Is this packet an acknowledgment?"""
        return self.pkt_type == ACK


    @property
    def is_err(self):
        """Is this packet reporting an error?"""
        return self.pkt_type == ERR


    @property
    def is_bye(self):
        """Is this packet signaling the end of a transfer?"""
        return self.pkt_type == BYE


    @property
    def is_con(self):
        """Is this packet continuing a frame?"""
        return self.frame_flag == CON


    @property
    def is_end(self):
        """Is this packet ending a frame?"""
        return self.frame_flag == END


    @classmethod
    def new(cls, typ, flag, seqn, seql, addr, data):
        """Easily construct a new packet instance"""
        # Check packet type
        if typ not in [REQ, CNF, DEN, SEQ, ACK, ERR, BYE]:
            raise ValueError('Packet type unknown: ' + typ)

        # Check frame type
        if flag not in [CON, END]:
            raise ValueError('Frame type unknown: ' + flag)

        # Check sequence number/length value
        if seqn > seql + 1:
            raise ValueError('Sequence# cannot be greater than Sequence length! (%i > %i)' % (seqn, seql))

        # Check seql/seqn range
        if seqn > math.pow(2, 32) - 1:
            raise ValueError("Sequence# won't! fit in 32 bits!")
        if seql > math.pow(2, 32) - 1:
            raise ValueError("Sequence length won't! fit in 32 bits!")

        # Check seql/seqn type
        if type(seqn) is not int:
            raise ValueError("Sequence# must be of type <int>")
        if type(seql) is not int:
            raise ValueError("Sequence length must be of type <int>")

        return cls(typ + flag + struct.pack('I', seqn) + struct.pack('I', seql) + data, addr)


def bytes_to_string(b):
    """Convert an iterable of bytes to a string"""
    return ''.join(map(chr, b))


def string_to_bytes(s):
    """Convert a string to list of bytes"""
    return map(ord, s)


def string_to_bytearray(s):
    """Convert a string to bytearray"""
    return bytearray(map(ord, s))


if __name__ == '__main__':
    # Do some test junk...
    p = Packet.new(REQ, CON, 1, 256, ('localhost', 1234), 'Hello, World!')
    print 'seq#:', p.seq_num,
    print p.is_req, p.is_cnf, p.is_den, p.is_seq, p.is_ack, p.is_err
    print p.data
    print [p._data]
