# CIS 457 Project 3: Reliability over UDP

* __Objective__: Implement a reliable file transfer program.
* __Deliverables__: You must turn in your code on bloackboard by 11:59 PM on 4/15/2013. Additionally, you must demo your program on 4/16/2013 in lab. If you do not demo, you will recieve no credit (all group members should be present). Your group evaluation forms are due in hard copy by 4/17/2103.
* This project should be done in groups of 4

## Authors
* Aaron Herndon
* Chris Carr
* Josh Friend
* Kyle Peltier

## Introduction
In lecture, we recently discussed the differences between TCP and UDP. TCP provides several services not provided by UDP, including reliability. In lab, we will discuss programming using UDP sockets. In this assignment, you will write a file transfer application that uses UDP, but with reliability. You may use Java, C, or C++. You may not use any non-standard libraries without prior permission. Aditionally, you will write a second program to analyze your protocol.

## Specifications
You must write the client and server to support your file transfer. The client should send to the server a request to initiate a transfer of a specific file. Upon recieving the request, if the file exists, it should be sent to the client. Be sure your program works with binary files, not just text files.

* When the server starts, it should wait for a client connection (on a port number either obtained as a command line option or input by the user).
* When the client starts, it should connect to the server and send a request for a file. This request may be formatted in any way your group finds convenient. The server IP address, server port number, and file name may either be taken by the client program as user input, or as command line options.
* Upon recieving a request, the server should read the requested file if it exists, and send it to the client, which should write the file locally. The file transfer must meet the following requirements:
    - UDP sockets must be used.
    - The data in each packet, including your header, must be 1024 bytes or less.
    - The file transfer must be reliable. It is up to you to determine how to make it reliable.
* You may not just implement a stop-and-wait protocol. You client must send multiple packets of data at once if a sufficiently large file is requested. You may impose a maximum on the number of outstanding packets, but this maximum must be at least 5.
* The client should each print to the screen each time they send or recieve a packet.

## Protocol analysis
In addition to the client/server described above, you must write a program to examing and analyze your custom reliability protocol. It need not do anything fancy, but must at least print out for each packet of your protocol, all of your custom header fields. This can be built either using the pcap library as we did in a prevoius lab, or as a custom plugin to wireshark (but the instructor has never made a wireshark plugin and so can not provide much technical support for this option).

## Testing
Instructions for how to simulate loss and delay of traffic in each direction will be posted here shortly.

## Grading

There will be a total of 20 points, divided as follows:

    Criteria                                        Points
    -------------------------------------------------------
    Code compiles without error .................... 1
    Transfer request ............................... 1
    Text file data transfer (no loss/reorder) ...... 1
    Binary file data transfer (no loss/reorder) .... 1
    Good design of custom header ................... 2
    Proper recovery from dropped data packets ...... 2
    Proper recovery from reordered data packets .... 2
    Proper recovery from dropped acknoledgments .... 2
    Proper recovery from reordered acknoledgments .. 2
    File reading/writing ........................... 1
    Runtime error handling ......................... 1
    Protocol analysis program ...................... 2
    Group evaluation ............................... 2
