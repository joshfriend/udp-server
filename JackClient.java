import java.io.*;
import java.net.*;



public class JackClient {

    //CONSTRUCTOR
    public JackClient(){    }

    /** RUN
     *   Set up client socket. Create send/receive packets. Get input from user
     *  indicating the file to get from server. Send request. Receive packets
     *  and process.
     */
    public void run() throws Exception{

        //create byte arrays to hold packet data
        byte[] sendData = new byte[TCodes.MAX_PACKET_SIZE];
        byte[] recvData = new byte[TCodes.MAX_PACKET_SIZE];

        String stringToSend;
        String stringReceived;

        //create udp client socket and send/receive packets
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress IPAddress = InetAddress.getByName("127.0.0.1");     //designate address to send data to (can take ip addresses or even dns host names)
        DatagramPacket sendPacket;
        DatagramPacket recvPacket= new DatagramPacket(recvData, recvData.length);

        //get data from user input and convert to byte array
        sendData = getUserInput();

        //create packet to send to server and send
        sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 9876);
        clientSocket.send(sendPacket);      //send packet over udp socket


        //receive 1st packet from server
        try{
            clientSocket.receive(recvPacket);
        }
        catch(SocketTimeoutException ste){      }
        catch(IOException ioe){}

        System.out.println("Packet length: " + recvPacket.getLength());     //print length of packet

        //get the string from the data
        stringReceived = new String(recvData);
        // stringReceived = stringReceived.trim();

        System.out.println("Got from server: " + stringReceived);

        //check if server has requested file
        //if server confirmed file, start retreiving packets. otherwise, abort
        if (stringReceived.substring(0,2).compareTo(TCodes.CNF)==0){
        	System.out.println("FILE CONFIRMED");
        	PacketHandler packHandler = new PacketHandler(clientSocket);
        	packHandler.process();
        }
        else{
        	System.out.println("FILE DOES NOT EXIST");
        }



        clientSocket.close();
    }


    /** GETUSERINPUT
     * 		prompts user for file to get from server. returns array of
     * 	bytes of custom header followed by user's input
     */
    public byte[] getUserInput(){
        System.out.print("Enter file you want-> ");
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));

        String input = TCodes.REQ + TCodes.END + "0000";
        try{
            input += inFromUser.readLine();
        }
        catch(IOException ioe){}
        return input.getBytes();        	//convert send data into bytes
    }


    /** MAIN FUNCTION
     *  	run the client
     */
    public static void main(String args[]) throws Exception{
        JackClient client = new JackClient();
        client.run();
    }
}
